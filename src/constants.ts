import { SemanticICONS } from "semantic-ui-react"

export enum PageRoute {
	HOME = '/',
	MARKET = '/market',
	ORDERBOOK = 'orderbook',
}


export const PageRouteNames: Record<PageRoute, string> = {
	[PageRoute.HOME]: 'Home',
	[PageRoute.MARKET]: 'Market',
	[PageRoute.ORDERBOOK]: 'Order Book',
}

export const PageRouteIcons: Record<PageRoute, SemanticICONS> = {
	[PageRoute.HOME]: 'home',
	[PageRoute.MARKET]: 'line graph',
	[PageRoute.ORDERBOOK]: 'book',
}

export const enum Expertise {
	AGRI = 'AGRICULTURE',
	CHEM = 'CHEMISTRY',
	CONST = 'CONSTRUCTION',
	ELEC = 'ELECTRONICS',
	FOOD = 'FOOD_INDUSTRIES',
	FUEL = 'FUEL_REFINING',
	MANF = 'MANUFACTURING',
	METL = 'METALLURGY',
	RXCT = 'RESOURCE_EXTRACTION',
	INFRA = 'INFRASTRUCTURE',
}


export const ExpertiseIcons: Record<Expertise, SemanticICONS> = {
	[Expertise.AGRI]: 'sun',
	[Expertise.CHEM]: 'lab',
	[Expertise.CONST]: 'wrench',
	[Expertise.ELEC]: 'microchip',
	[Expertise.FOOD]: 'food',
	[Expertise.FUEL]: 'rocket',
	[Expertise.MANF]: 'factory',
	[Expertise.METL]: 'magnet',
	[Expertise.RXCT]: 'angle double up',
	[Expertise.INFRA]: 'building',
}