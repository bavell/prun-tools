import React from 'react'
import { createRoot } from 'react-dom/client'
import { Provider } from 'react-redux'
import { Route, BrowserRouter, Routes } from 'react-router-dom'

import Store from './state'
import { Home } from './routes/Home'
import { PageWrapper } from './components/Page'
import { QuotesPage } from './routes/Quotes'
import { PageRoute } from '@constants'
import { Market } from './routes/Market'

function render() {
	createRoot(document.getElementById('root')).render(
		<Provider store={Store}>
			<BrowserRouter basename='/'>
				<PageWrapper>
					<Routes>
						<Route index element={<Home />} />
						<Route element={<QuotesPage />} path={PageRoute.ORDERBOOK} />
						<Route element={<Market />} path={PageRoute.MARKET} />
					</Routes>
				</PageWrapper>
			</BrowserRouter>
		</Provider>
	)
}

render()
