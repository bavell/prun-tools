import {Expertise} from './constants'

declare global {
	interface DataState<Data> {
		error: string | null
		initialized: boolean
		loading: boolean
		data: Data
	}

	type WithDataState<T> = {
		[K in keyof T]: DataState<T[K]>
	}

	interface EntityMeta {
		id: string
		name: string
		created: number
		updated: number
		archived?: number
	}

	interface Building {
		name?: string
		code: string
		area: number
		expertise: Expertise
		workforce: WorkForce
		bom: BOMLineItem[]
	}

	interface WorkForce {
		pio: number
		set: number
		tech: number
		eng: number
		sci: number
	}

	interface BOMLineItem {
		matCode: string
		qty: number
	}

	interface Material {
		id: string
		name: string
		ticker: string
		category: MaterialCategory
		weight: number
		volume: number
	}

	interface MaterialCategory {
		name: string
		id: string
	}

	interface MaterialMarketData {
		ticker: string
		ai1: MarketPriceData
		ci1: MarketPriceData
		nc1: MarketPriceData
		ic1: MarketPriceData
	}
	interface MarketPriceData {
		avg: number
		askPrice: number
		bidPrice: number
	}

	interface PlanetConditions {
		surface: PlanetSurface
		grav: PlanetConditionOptions
		temp: PlanetConditionOptions
		atm: PlanetConditionOptions
	}

	type PlanetSurface = 'rocky' | 'gaseous'
	type PlanetConditionOptions = 'low' | 'mid' | 'high'
}
