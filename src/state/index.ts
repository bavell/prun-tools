import { useSelector, TypedUseSelectorHook, useDispatch } from 'react-redux'
import { configureStore, bindActionCreators, ActionCreatorsMapObject } from '@reduxjs/toolkit'
import {reducer as fio} from '../modules/fio/state'

const Store = configureStore({
	reducer: {
		fio,
	},
})


export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export const useAppDispatch = function () {
	return useDispatch<typeof Store['dispatch']>()
}
export const useAppDispatcher = function <
	A extends ActionCreatorsMapObject<any>
>(actions: A) {
	const dispatcher = useAppDispatch()
	return bindActionCreators(actions, dispatcher)
}

export default Store
export type RootState = ReturnType<typeof Store.getState>