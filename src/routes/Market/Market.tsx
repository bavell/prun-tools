import React from 'react'
import { Button, Container, Form, Header, Loader, Menu, Placeholder, Segment, Table } from 'semantic-ui-react'
import { Page } from '../../components/Page'
import { useAppDispatch, useAppSelector } from '../../state'

import { useForm, Resolver, FormProvider } from 'react-hook-form'
import { DropdownQuestion } from '../../components/Form/DropdownQuestion'
import { fetchPricingData, formatCurrency, selectFIOState } from '../../modules/fio'

interface FormValues {
	tickers: string[]
}

type Props = {}
export const Market: React.FC<Props> = function (props) {
	const { matPricing: {data: priceData, loading} } = useAppSelector(selectFIOState)
	const dispatch = useAppDispatch()

	function refresh() {
		dispatch(fetchPricingData())
	}

	const form = useForm<FormValues>({
		defaultValues: {
			tickers: [],
		},
	})
	const onSubmit = form.handleSubmit((data) => {
		console.log(data)
	})

	const { tickers } = form.watch()

	const filtered = priceData.filter((x) => {
		return !tickers.length || tickers.includes(x.ticker)
	})

	return (
		<Page name='home' title='Market' icon='line graph'>
			<Segment attached='top'>
				<Header content='Filters' icon='filter' />
				<FormProvider {...form}>
					<Form onSubmit={onSubmit}>
						<DropdownQuestion
							name='tickers'
							label='Tickers'
							search
							multiple
							placeholder='Filter by Tickers...'
							options={priceData.map((x) => ({
								text: x.ticker,
								value: x.ticker,
								key: x.ticker,
							}))}
						/>
						<Form.Group>
							{/* <Form.Button type='submit' content='Search' icon='search' /> */}
							<Form.Button type='button' content='Clear Filters' icon='trash' onClick={() => form.reset()} />
						</Form.Group>
					</Form>
				</FormProvider>
			</Segment>
			<Menu attached>
				<Menu.Item>
					<Button
						type='button'
						content='Refresh Market Data'
						color='blue'
						icon='refresh'
						onClick={refresh}
						loading={loading}
						disabled={loading}
					/>
				</Menu.Item>
			</Menu>
			{!loading && (
				<Table color='green' attached='bottom'>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell content='Ticker' />
							<Table.HeaderCell content='AI1 Avg' />
							<Table.HeaderCell content='CI1 Avg' />
							<Table.HeaderCell content='NC1 Avg' />
							<Table.HeaderCell content='IC1 Avg' />
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{filtered.map((row) => (
							<Table.Row key={row.ticker}>
								<Table.Cell content={`${row.ticker}`} />
								<Table.Cell content={formatCurrency(row.ai1.avg)} />
								<Table.Cell content={formatCurrency(row.ci1.avg)} />
								<Table.Cell content={formatCurrency(row.nc1.avg)} />
								<Table.Cell content={formatCurrency(row.ic1.avg)} />
							</Table.Row>
						))}
					</Table.Body>
				</Table>
			)}
			{!!loading && (
				<Segment loading attached='bottom'>
					<Container text textAlign='center'>
					<Placeholder fluid>
						<Placeholder.Header image>
							<Placeholder.Line />
							<Placeholder.Line />
						</Placeholder.Header>
						<Placeholder.Paragraph>
							<Placeholder.Line />
							<Placeholder.Line />
							<Placeholder.Line />
						</Placeholder.Paragraph>
					</Placeholder>
					</Container>
				</Segment>
			)}
		</Page>
	)
}
