import { ExpertiseIcons } from '@constants'
import { selectBuildingCodeMap, selectFIOAllData, selectFIOState, useAppSelector } from '@modules/fio'
import { sumBy } from 'lodash'
import sum from 'lodash/sum'
import React, { useState } from 'react'
import {
	Button,
	Container,
	Dropdown,
	Form,
	Grid,
	Header,
	Icon,
	Input,
	Label,
	Placeholder,
	Segment,
	SemanticCOLORS,
	Statistic,
	Table,
} from 'semantic-ui-react'
import { Page } from '../../components/Page'

import styles from './ecc.module.scss'

// Reference: https://handbook.apex.prosperousuniverse.com/wiki/building-costs/
// Reference: https://fio.fnar.net/planetsearch?Resources=FEO&Rocky=true&Gas=true&LowGrav=true&HighGrav=true&LowPres=true&HighPres=true&LowTemp=true&HighTemp=true


// NEXT STEPS
// Rebuild UI to integrate with new redux state: Module.Quotes.State
// Base Planner Page - add planets you own and the buildings on them to see:
//  - construction costs (to see expansion costs of adding a new building)
//  - consumable upkeep by pop tier & planet, as well as global total


type Props = {}
export const QuotesPage: React.FC<Props> = function (props) {
	const { buildings } = useAppSelector(selectFIOAllData)
	// const grouped = useAppSelector(selectGroupedBuildings)
	const BuildingCodeMap = useAppSelector(selectBuildingCodeMap)

	// const [bldgDropdownOpen, setBldgDropdownOpen] = useState<boolean>(false)
	const [qty, setQty] = useState<number>(1)
	const [selectedBldg, setSelectedBldg] = useState<string | null>(null)
	const [bldgList, setBldgList] = useState<{ bldg: Building; qty: number }[]>([])

	return (
		<Page name='orderbook' title='Order Book' icon='book'>
			<Segment color='blue' padded>
				
				<Segment>
					<Form
						onSubmit={(x) => {
							console.log(selectedBldg)
							setBldgList([
								...bldgList,
								{
									bldg: BuildingCodeMap[selectedBldg],
									qty,
								},
							])
							setSelectedBldg(null)
							setQty(1)
						}}>
						<Header content='Buildings' />
						<Form.Group>
							<Form.Field>
								<Input
									name='bldg_qty'
									type='number'
									placeholder='Quantity'
									value={qty}
									onChange={(e, { value }) => setQty(parseInt(value))}
									action>
									<input />
									<Dropdown
										name='bldg_code'
										search
										selection
										placeholder='Select a building...'
										value={selectedBldg}
										text={selectedBldg}
										onChange={(e, x) => {
											console.log('ONCHANBGE DROPDOWN', x.value)
											setSelectedBldg(x.value as string)
										}}
										options={buildings.map((b) => ({
											key: b.code,
											value: b.code,
											text: `${b.code}`,
											icon: ExpertiseIcons[b.expertise],
										}))}
										closeOnBlur
										closeOnChange
										closeOnEscape
									/>
									<Button content='Add' icon='plus' color='green' type='submit' />
								</Input>
							</Form.Field>
						</Form.Group>
					</Form>
					<Statistic.Group>
						<Statistic label='# of Buildings' value={sumBy(bldgList, (x) => x.qty)} />
						<Statistic label='Total Area' value={sumBy(bldgList, (x) => x.bldg.area * x.qty)} />
						<Statistic label='Total PIO' value={sumBy(bldgList, (x) => x.bldg.workforce.pio * x.qty)} />
						<Statistic label='Total SET' value={sumBy(bldgList, (x) => x.bldg.workforce.set * x.qty)} />
					</Statistic.Group>
					<Table>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell content={`Qty`} />
								<Table.HeaderCell content={`Building`} />
								{/* <Table.HeaderCell content={` `} /> */}
							</Table.Row>
						</Table.Header>
						<Table.Body>
							{bldgList.map((row) => {
								return (
									<Table.Row key={row.bldg.code}>
										<Table.Cell>
											<span>
												<Icon link name='minus' color='red' />
												<Icon link name='plus' color='green' />
											</span>
											{row.qty}
										</Table.Cell>
										<Table.Cell>
											<Icon name={ExpertiseIcons[row.bldg.expertise]} />
											{row.bldg.code}
										</Table.Cell>
										{/* <Table.Cell content={''} /> */}
									</Table.Row>
								)
							})}
						</Table.Body>
					</Table>

					<div className={styles.bomList}>
							{}
					</div>
				</Segment>
					<Table color='green' attached='bottom'>
						<Table.Header>
							<Table.Row>
								<Table.HeaderCell content='Ticker' />
								<Table.HeaderCell content='Expertise' />
								<Table.HeaderCell content='Area' />
								<Table.HeaderCell content='Workforce Total' />
								<Table.HeaderCell content='Workforce' />
								<Table.HeaderCell content='Construction' />
								{/* <Table.HeaderCell content='Cost Estimates' /> */}
							</Table.Row>
						</Table.Header>
						<Table.Body>
							{buildings.map((row) => (
								<Table.Row key={row.code}>
									<Table.Cell content={row.code} />
									<Table.Cell content={row.expertise} />
									<Table.Cell content={row.area} />
									<Table.Cell content={sum(Object.values(row.workforce))} />
									<Table.Cell
										content={
											<>
												{Object.entries(row.workforce)
													.filter((x) => x[1] > 0)
													.map((wf) => (
														<Label
															key={wf[0]}
															content={`${wf[0].toUpperCase()}: ${wf[1]}`}
															color={((): SemanticCOLORS => {
																if (wf[0] === 'pio') return undefined
																if (wf[0] === 'set') return 'blue'
																if (wf[0] === 'tech') return 'orange'
																if (wf[0] === 'eng') return 'red'
																if (wf[0] === 'sci') return 'purple'
															})()}
														/>
													))}
											</>
										}
									/>
									<Table.Cell
										content={
											<>
												{row.bom.map((bom) => (
													<Label
														key={bom.matCode}
														content={`${bom.matCode}: ${bom.qty}`}
														color={((): SemanticCOLORS => {
															if (bom.matCode === 'TRU') return 'teal'
															if (bom.matCode[0] === 'B') return undefined
															if (bom.matCode[0] === 'L') return 'blue'
															if (bom.matCode[0] === 'A') return 'orange'
															if (bom.matCode[0] === 'R') return 'red'
															if (bom.matCode[0] === 'S') return 'purple'

															return undefined
														})()}
													/>
												))}
											</>
										}
									/>
									{/* <Table.Cell
										content={
											<>
												{Object.entries({}).map((group) => (
													<Label
														tag
														key={}
														content={`${bom.matCode}: ${bom.qty}`}
														color={((): SemanticCOLORS => {
															if (bom.matCode === 'TRU') return 'teal'
															if (bom.matCode[0] === 'B') return undefined
															if (bom.matCode[0] === 'L') return 'blue'
															if (bom.matCode[0] === 'A') return 'orange'
															if (bom.matCode[0] === 'R') return 'red'
															if (bom.matCode[0] === 'S') return 'purple'

															return undefined
														})()}
													/>
												))}
											</>
										}
									/> */}
								</Table.Row>
							))}
						</Table.Body>
					</Table>

				{/* {!!loading && (
					<Segment loading attached='bottom'>
						<Container text textAlign='center'>
							<Placeholder fluid>
								<Placeholder.Header image>
									<Placeholder.Line />
									<Placeholder.Line />
								</Placeholder.Header>
								<Placeholder.Paragraph>
									<Placeholder.Line />
									<Placeholder.Line />
									<Placeholder.Line />
								</Placeholder.Paragraph>
							</Placeholder>
						</Container>
					</Segment>
				)} */}
				{/* <code>
					<pre>{JSON.stringify(buildings, undefined, '\t')}</pre>
				</code> */}
			</Segment>
		</Page>
	)
}
