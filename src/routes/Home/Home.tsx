import React from 'react'
import { Header, Segment, Table } from 'semantic-ui-react'
import { Page } from '../../components/Page'

type Props = {}
export const Home: React.FC<Props> = function (props) {
	return (
		<Page name='home' title='Hello World!' icon='home'>
			<Segment color='blue' padded>
				<Header content='Hello World!' />
			</Segment>
		</Page>
	)
}