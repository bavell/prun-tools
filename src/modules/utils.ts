import {v4} from 'uuid'
export {v4 as genID}

export function initDataState<D extends object>(): DataState<D[]> {
	return {
		error: null,
		initialized: false,
		loading: false,
		data: [] as D[]
	}
}

export function extractDataState<D extends DataState<D[]>>(d: D): D['data'] {
	return d.data
}