import { genID } from '@modules/utils'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState: Module.Quotes.State = {
	quotes: [],
}

export const slice = createSlice({
	name: 'orderbook',
	initialState,
	reducers: {
		initData: (state, action: PayloadAction<Module.Quotes.State>) => {
			state = action.payload
		},

		addQuote: (state, action: PayloadAction<Omit<Module.Quotes.Quote, keyof EntityMeta>>) => {
			state.quotes.push({
				...action.payload,
				id: genID(),
				name: '',
				created: Date.now(),
				updated: Date.now(),
			})
		},
	},
})

export const { actions, reducer } = slice
export default slice
