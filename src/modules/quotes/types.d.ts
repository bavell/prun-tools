
declare namespace Module {
	namespace Quotes {
		interface State {
			quotes: Quote[]
		}

		interface Quote extends EntityMeta {
			materials: MaterialOrder
			buildings: BuildingOrder
		}

		interface MaterialOrder {
			mat: Material
			qty: number

			/**
			 * Price per unit
			 * If specified, the total order amount = qty * unitPrice
			 */
			unitPrice?: number

			/**
			 * Price for entire order
			 * If specified, this is the total order amount
			 */
			orderPrice?: number
		}

		interface BuildingOrder {
			building: Building
			qty: number
		}

	}
}