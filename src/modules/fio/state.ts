import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import orderBy from 'lodash/orderBy'
import { fetchBuildingData, fetchPricingData, fetchMaterialData } from './fio'
import { initDataState } from '@modules/utils'

const initialState: Module.FIO.State = {
	initialized: false,
	matPricing: initDataState<MaterialMarketData>(),
	buildings: initDataState<Building>(),
	matCategories: initDataState<MaterialCategory>(),
	materials: initDataState<Material>(),
}

export const slice = createSlice({
	name: 'fio',
	initialState,
	reducers: {},
	extraReducers(builder) {
		builder
			.addCase(fetchPricingData.pending, (state, action) => {
				state.matPricing.loading = true
			})
			.addCase(fetchPricingData.fulfilled, (state, action) => {
				state.matPricing.loading = false
				state.matPricing.initialized = true
				state.matPricing.data = action.payload
			})
			.addCase(fetchPricingData.rejected, (state, action) => {
				state.matPricing.loading = false
				state.matPricing.error = action.error.message
			})

			.addCase(fetchBuildingData.pending, (state, action) => {
				state.buildings.loading = true
			})
			.addCase(fetchBuildingData.fulfilled, (state, action) => {
				state.buildings.loading = false
				state.buildings.initialized = true
				state.buildings.data = orderBy(
					action.payload,
					[(b) => `${b.expertise === 'INFRASTRUCTURE' ? 'zz' : ''}${b.expertise}`, (b) => b.code],
					['asc', 'asc']
				)
			})
			.addCase(fetchBuildingData.rejected, (state, action) => {
				state.buildings.loading = false
				state.buildings.error = action.error.message
			})

			.addCase(fetchMaterialData.pending, (state, action) => {
				state.materials.loading = true
				state.matCategories.loading = true
			})
			.addCase(fetchMaterialData.fulfilled, (state, action) => {
				state.materials.loading = false
				state.materials.initialized = true
				state.materials.data = action.payload[1]

				state.matCategories.loading = false
				state.matCategories.initialized = true
				state.matCategories.data = action.payload[0]
			})
			.addCase(fetchMaterialData.rejected, (state, action) => {
				state.materials.loading = false
				state.materials.error = action.error.message

				state.matCategories.loading = false
				state.matCategories.error = action.error.message
			})

			.addMatcher(
				(action) => action.type.endsWith('/fulfilled'),
				(state) => {
					state.initialized =
						state.buildings.initialized &&
						state.matCategories.initialized &&
						state.matPricing.initialized &&
						state.materials.initialized
				}
			)
	},
})

export const { actions, reducer } = slice
export default slice
