import { createAsyncThunk,  } from '@reduxjs/toolkit'
import {parse} from 'csv-parse/lib/sync'
import { find, groupBy, map } from 'lodash'
import { useEffect } from 'react'
import { useAppDispatch, useAppDispatcher, useAppSelector } from '../../state'
import { selectFIOState } from './selectors'
import { actions } from './state'


export async function useFIOData() {
	const $state = useAppSelector(selectFIOState)
	const dispatch = useAppDispatch()

	useEffect(() => {
		if (!$state.initialized) {
			dispatch(fetchPricingData())
			dispatch(fetchBuildingData())
			dispatch(fetchMaterialData())
		}
	}, [$state.initialized])
}

async function fetchFIOCSV(url: string) {
	return fetch(url, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/csv'
		},
		credentials: 'omit'
	})
}

async function fetchFIOJSON(url: string) {
	return fetch(url, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		},
		credentials: 'omit'
	})
}

export const fetchPricingData = createAsyncThunk<MaterialMarketData[]>('fio/fetchPricingData', async () => {
  const result = await fetchFIOCSV('https://rest.fnar.net/csv/prices')
  return parseMarketData(await result.text())
})

function parseMarketData(rawData: string): MaterialMarketData[] {
	const parsed = parse(rawData, {
		columns: true,
		skip_empty_lines: true,
	}) as object[]

	console.log(parsed[0])

	return parsed.map(x => ({
		ticker: x['Ticker'],
		ai1: parsePriceData(x, 'AI1'),
		ci1: parsePriceData(x, 'CI1'),
		nc1: parsePriceData(x, 'NC1'),
		ic1: parsePriceData(x, 'IC1'),
	}))
}

function parsePriceData(data: object, marketTicker: string): MarketPriceData {
	return {
		avg: normalizeNum(data[`${marketTicker}-Average`]),
		askPrice: normalizeNum(data[`${marketTicker}-AskPrice`]),
		bidPrice: normalizeNum(data[`${marketTicker}-BidPrice`]),
	}
}

export const fetchBuildingData = createAsyncThunk<Building[]>('fio/fetchBuildingData', async () => {
  const result = await fetchFIOJSON('https://rest.fnar.net/building/allbuildings')
  return parseBuildingData(await result.json())
})


function parseBuildingData(rawData: object[]): Building[] {
	return rawData.map(x => ({
		// name: x['name'],
		code: x['Ticker'],
		area: x['AreaCost'],
		expertise: x['Expertise'] || 'INFRASTRUCTURE',
		workforce: {
			pio: x['Pioneers'],
			set: x['Settlers'],
			tech: x['Technicians'],
			eng: x['Engineers'],
			sci: x['Scientists'],
		},
		bom: (x['BuildingCosts'] || []).map(c => ({
			matCode: c['CommodityTicker'],
			qty: c['Amount']
		}))
	}))
}

export const fetchMaterialData = createAsyncThunk<[MaterialCategory[], Material[]]>('fio/fetchMaterialData', async () => {
  const result = await fetchFIOJSON('https://rest.fnar.net/material/allmaterials')
  return parseMaterialData(await result.json())
})


function parseMaterialData(rawData: object[]): [MaterialCategory[], Material[]] {
	const categories: MaterialCategory[] = map(groupBy(rawData, 'CategoryId'), (mats, catID): MaterialCategory => {
		return {
			id: catID,
			name: mats[0]['CategoryName']
		}
	})

	const materials: Material[] = rawData.map(x => ({
		id: x['MaterialId'],
		name: x['name'],
		ticker: x['Ticker'],
		weight: x['Ticker'],
		volume: x['Ticker'],
		category: find(categories, c => c.id === x['CategoryId']),
	}))

	return [categories, materials]
}


function normalizeNum(str: string): number {
	return str === '' ? 0 : (Math.round(parseFloat(str)))
}


const currencyFormatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	maximumFractionDigits: 0,
	minimumFractionDigits: 0,
})


export function formatCurrency(amount: number, stripSymbol = false) {
	let rv = currencyFormatter.format(amount)
	if (!stripSymbol) return rv
	else return amount > 0 ? rv.slice(1) : `-${rv.slice(2)}`
}