import { createSelector } from '@reduxjs/toolkit'
import groupBy from 'lodash/groupBy'
import { RootState } from '../../state'
export { useAppSelector } from '../../state'

/**
 * Prop Access
 */
export const selectFIOState = (state: RootState) => state.fio
export const selectFIOPricing = (state: RootState) => state.fio.matPricing.data
export const selectFIOBuildings = (state: RootState) => state.fio.buildings.data
export const selectFIOMaterials = (state: RootState) => state.fio.materials.data
export const selectFIOMaterialCategories = (state: RootState) => state.fio.matCategories.data

/**
 * Prop Assembly
 */
export const selectFIOAllData = (state: RootState): Module.FIO.StateData => ({
	buildings: state.fio.buildings.data,
	matCategories: state.fio.matCategories.data,
	materials: state.fio.materials.data,
	matPricing: state.fio.matPricing.data
})


/**
 * Selectors
 */
export const buildPricingTable = createSelector([selectFIOState], (fio) => {
	return fio.matPricing.data.reduce((acc, mat) => {
		acc[mat.ticker] = mat
		return acc
	}, {} as Record<string, MaterialMarketData>)
})

export const selectGroupedBuildings = createSelector([selectFIOBuildings], (buildings) => {
	return groupBy(buildings, (x) => x.expertise || 'INFRASTRUCTURE')
})

export const selectBuildingCodeMap = createSelector([selectFIOBuildings], (buildings) => {
	return buildings.reduce((acc, item) => {
		acc[item.code] = item
		return acc
	}, {} as Record<string, Building>)
})
