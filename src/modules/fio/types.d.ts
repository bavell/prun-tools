
declare namespace Module {
	namespace FIO {
		interface State extends WithDataState<StateData> {
			initialized: boolean
		}

		interface StateData {
			matPricing: MaterialMarketData[]
			buildings: Building[]
			materials: Material[]
			matCategories: MaterialCategory[]
		}
	}
}