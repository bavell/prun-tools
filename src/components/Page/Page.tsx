import React from 'react'
import { Segment, Header, SemanticICONS, Icon } from 'semantic-ui-react'

import styles from './Page.module.scss'

type Props = React.PropsWithChildren<
	| {
			name: string
			title: never
			icon: never
	  }
	| {
			name: string
			title?: React.ReactNode
			icon?: SemanticICONS
	  }
>
export const Page: React.FC<Props> = function ({ children, name, title, icon }) {
	return (
		<Segment basic className={`${styles.page} ${name}`}>
			{!!title && (
				<Header size='huge'>
					{!!icon && <Icon name={icon} />}
					<Header.Content>{title}</Header.Content>
				</Header>
			)}
			{children}
		</Segment>
	)
}
