import React from 'react'
import { Container, Segment } from 'semantic-ui-react'
import { useFIOData } from '../../modules/fio'
import { useAppSelector } from '../../state'

import { NavMenu } from '../NavMenu'
import styles from './Page.module.scss'

type Props = React.PropsWithChildren<{}>
export const PageWrapper: React.FC<Props> = function (props) {
	const { initialized } = useAppSelector((s) => s.fio)

	useFIOData()

	return (
		<div className={styles.pageWrapper}>
			<div className={styles.sidebar}>
				<NavMenu inverted vertical fluid />
			</div>
			<div className={styles.pageContent}>
				<Container fluid>
					{!!initialized && props.children}
					{!initialized && <Segment loading>&nbsp;</Segment>}
				</Container>
			</div>
		</div>
	)
}
