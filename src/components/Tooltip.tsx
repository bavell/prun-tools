import React from 'react'
import { Header, Icon, List, Popup, PopupProps, SemanticCOLORS, SemanticICONS } from 'semantic-ui-react'
import { IconSizeProp } from 'semantic-ui-react/dist/commonjs/elements/Icon/Icon'

type BaseTooltipProps = {
	title: string
	id?: string
	size?: IconSizeProp
	className?: string
	extraProps?: PopupProps
}

type TooltipProps = {
	icon: SemanticICONS
	color: SemanticCOLORS
} & BaseTooltipProps
export const Tooltip: React.FC<React.PropsWithChildren<TooltipProps>> = function (props) {
	const { title, icon, color, children, size, ...rest } = props
	return (
		<Popup
			flowing
			hoverable
			trigger={<Icon name={icon} color={color} style={{ fontSize: size ? undefined : '.8em' }} fitted size={size} />}
			{...rest}
			{...props.extraProps}>
			<Header content={title} />
			{children}
		</Popup>
	)
}

type InfoTooltipProps = {} & BaseTooltipProps
export const InfoTooltip: React.FC<InfoTooltipProps> = function (props) {
	return <Tooltip color='blue' icon='question circle' {...props} />
}

type WarningTooltipProps = {} & BaseTooltipProps
export const WarningTooltip: React.FC<WarningTooltipProps> = function (props) {
	return <Tooltip color='orange' icon='warning sign' {...props} />
}

type ErrorTooltipProps = {} & BaseTooltipProps
export const ErrorTooltip: React.FC<ErrorTooltipProps> = function (props) {
	return <Tooltip color='red' icon='warning circle' {...props} />
}
