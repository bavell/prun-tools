import React from 'react'
import {} from 'react-router-dom'
import { Form, Header } from 'semantic-ui-react'

type Props = {}
export const PlanetConditionsForm: React.FC<Props> = function (props) {
	return (
		<Form>
			<Form.Group inline>
				<Form.Field>
					<Header content='Planet Surface' />
					<Form.Group grouped>
						<Form.Field>
							<Form.Radio name='surface' label='Rocky' value='rocky' id='rocky' slider checked />
							<Form.Radio name='surface' label='Gaseous' value='gaseous' id='gaseous' slider />
						</Form.Field>
					</Form.Group>
				</Form.Field>

				<Form.Field>
					<Header content='Gravity' />
					<Form.Group grouped>
						<Form.Radio name='grav' label='Low' value='low' id='grav_low' slider />
						<Form.Radio name='grav' label='Mid' value='mid' id='grav_mid' slider checked />
						<Form.Radio name='grav' label='High' value='high' id='grav_high' slider />
					</Form.Group>
				</Form.Field>

				<Form.Field>
					<Header content='Temperature' />
					<Form.Group grouped>
						<Form.Radio name='temp' label='Low' value='low' id='temp_low' slider />
						<Form.Radio name='temp' label='Mid' value='mid' id='temp_mid' slider checked />
						<Form.Radio name='temp' label='High' value='high' id='temp_high' slider />
					</Form.Group>
				</Form.Field>

				<Form.Field>
					<Header content='Atmosphere' />
					<Form.Group grouped>
						<Form.Radio name='atmos' label='Low' value='low' id='atmos_low' slider />
						<Form.Radio name='atmos' label='Mid' value='mid' id='atmos_mid' slider checked />
						<Form.Radio name='atmos' label='High' value='high' id='atmos_high' slider />
					</Form.Group>
				</Form.Field>
			</Form.Group>
			<Form.Group>
				<Form.Radio
					name='use_planet_conditions'
					label='Use Planet Conditions in Building Costs'
					id='use_planet_conditions'
					toggle
					onClick={(e, data) => {
						console.log(data.checked)
					}}
				/>
			</Form.Group>
		</Form>
	)
}
