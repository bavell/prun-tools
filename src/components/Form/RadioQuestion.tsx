/*eslint-env browser*/

import React, { ReactElement, useState } from 'react'
import { useFormContext } from 'react-hook-form'
import {
	Form,
	Segment,
	Button,
	Icon,
	Input,
	Header,
	Placeholder,
	Message,
	Dropdown,
	Select,
	Checkbox,
	Radio,
	Ref,
	SemanticWIDTHS,
} from 'semantic-ui-react'
import { Question } from './Question'

type Props = {
	name: string
	placeholder?: string
	label?: string
	inputLabel?: string
	width?: SemanticWIDTHS
	inline?: boolean
	choices: {
		label: string
		value: string | number
	}[]
}

export const RadioQuestion: React.FC<Props> = function (props) {
	const { name, choices, placeholder, inline, label, inputLabel, width } = props
	const { setValue, resetField } = useFormContext()

	return (
		<Question
			name={name}
			render={({ error, onChange, ...fieldProps }) => (
				<Form.Field error={error?.message} width={width} inline={inline}>
					{!!label && <label>{label}</label>}
					{choices.map((choice, key) => (
						<Checkbox
							key={key}
							radio
							placeholder={placeholder}
							label={choice.label}
							{...fieldProps}
							value={choice.value}
							checked={choice.value === fieldProps.value}
							onChange={(event, data) => {
								setValue(name, data.value)
								// else resetField(name)
							}}
						/>
					))}
				</Form.Field>
			)}
		/>
	)
}
