/*eslint-env browser*/

import React, { ReactElement, useState } from 'react'
import {
	Form,
	Segment,
	Button,
	Icon,
	Input,
	Header,
	Placeholder,
	Message,
	Dropdown,
	Select,
	Checkbox,
	Radio,
	Ref,
	SemanticWIDTHS,
} from 'semantic-ui-react'
import { Question } from './Question'

type Props = {
	name: string
	placeholder?: string
	label?: string
	inputLabel?: string
	width?: SemanticWIDTHS
}

export const CheckboxQuestion: React.FC<Props> = function (props) {
	const { name, placeholder, label, inputLabel, width } = props

	return (
		<Question
			name={name}
			render={({ error, ...props }) => (
				<Form.Field error={error?.message} width={width}>
					{!!label && <label>{label}</label>}
					<Checkbox placeholder={placeholder} label={inputLabel} {...props} />
				</Form.Field>
			)}
		/>
	)
}
