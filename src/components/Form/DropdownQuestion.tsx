/*eslint-env browser*/

import React from 'react'
import { Form, Select, SemanticWIDTHS, StrictDropdownProps } from 'semantic-ui-react'
import { Question } from './Question'

type Props = {
	name: string
	options: NonNullable<StrictDropdownProps['options']>
	placeholder?: string
	label?: string
	multiple?: boolean
	required?: boolean
	search?: boolean
	width?: SemanticWIDTHS
}

export const DropdownQuestion: React.FC<Props> = function (props) {
	const { name, options, placeholder, label, multiple, search, width, required } = props
	return (
		<Question
			name={name}
			render={({ error, onChange, ...props }) => (
				<Form.Field error={error?.message} width={width} required={required}>
					{!!label && <label>{label}</label>}
					<Select
						{...props}
						placeholder={placeholder}
						options={options}
						multiple={multiple}
						search={search}
						onChange={(e, { name, value }) => {
							onChange(value)
						}}
					/>
				</Form.Field>
			)}
		/>
	)
}
