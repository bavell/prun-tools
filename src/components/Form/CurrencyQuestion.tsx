/*eslint-env browser*/

import React, { useRef } from 'react'
import { Form, Input, SemanticWIDTHS } from 'semantic-ui-react'
import { formatCurrency } from 'shared/utils'
import { Question } from './Question'

const MAX_VAL = 1e15

type Props = {
	name: string
	placeholder?: string
	label?: string
	inputLabel?: string
	width?: SemanticWIDTHS
	step?: number
	inline?: boolean
	fluid?: boolean
	min?: number
	max?: number
}

export const CurrencyQuestion: React.FC<Props> = function (props) {
	const { name, label, inputLabel, width, inline, ...rest } = props
	let inputRef = useRef<Input>(null)

	return (
		<Question
			name={name}
			render={({ error, ...fieldProps }) => (
				<Form.Field className='currency-question' error={error?.message} width={width} inline={inline}>
					{!!label && <label>{label}</label>}
					<Input
						ref={inputRef}
						fluid
						type={'text'}
						icon={'dollar'}
						iconPosition={'left'}
						label={inputLabel}
						max={MAX_VAL}
						placeholder='0'
						{...rest}
						{...fieldProps}
						value={format(fieldProps.value)}
						onChange={(e, field) => {
							const element = e.target
							const caret = element.selectionStart
							const val = transform(field.value)
							const commas = formatCurrency(fieldProps.value, true).replace(/[^,]/g, '').length
							const isMinusSign = field.value === '-'

							fieldProps.onChange(!isMinusSign ? normalize(val) : '-')

							const newCommas = formatCurrency(val, true).replace(/[^,]/g, '').length
							window.requestAnimationFrame(() => {
								element.selectionStart = (caret ?? 0) + (newCommas - commas)
								element.selectionEnd = (caret ?? 0) + (newCommas - commas)
							})
						}}
						// onClick={(e: React.MouseEvent) => {
						// 	inputRef.current?.focus()
						// 	console.log('CLICK')
						// 	inputRef.current?.select()
						// }}
					/>
				</Form.Field>
			)}
		/>
	)
}

function format(val: string | number) {
	if (val === '-')
		return val
	let rv = parse(val)
	return isNaN(rv) || rv === 0 ? '' : formatCurrency(rv, true)
}

function transform(val: string) {
	let rv = parse(val)
	return isNaN(rv) ? 0 : rv
}

function parse(val: string | number) {
	return parseInt(`${val}`.replace(/[^-0-9]*/g, '').trim())
}

function normalize(num: number) {
	if (isNaN(num)) num = 0
	if (num > MAX_VAL) num = MAX_VAL
	if (num < -MAX_VAL) num = -MAX_VAL
	return num
}
