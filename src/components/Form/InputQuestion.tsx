/*eslint-env browser*/

import React from 'react'
import { Form, Input, SemanticWIDTHS } from 'semantic-ui-react'
import { Question } from './Question'

type Props = {
	name: string
	placeholder?: string
	label?: string
	inputLabel?: string
	width?: SemanticWIDTHS
	number?: boolean
	step?: number
	currency?: boolean
	percentage?: boolean
	inline?: boolean
	fluid?: boolean
	required?: boolean
}

export const InputQuestion: React.FC<Props> = function (props) {
	const { name, placeholder, label, inputLabel, width, number, step, currency, percentage, inline, fluid,required } = props

	return (
		<Question
			name={name}
			render={({ error, ...props }) => (
				<Form.Field error={error?.message} width={width} inline={inline} required={required}>
					{!!label && <label>{label}</label>}
					<Input
						required={required}
						fluid={fluid}
						type={currency || percentage || number ? 'number' : 'text'}
						icon={currency ? 'dollar' : percentage ? 'percent' : undefined}
						step={step}
						iconPosition={currency || percentage ? 'left' : undefined}
						label={inputLabel}
						placeholder={placeholder}
						{...props}
					/>
				</Form.Field>
			)}
		/>
	)
}
