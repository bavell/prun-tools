/*eslint-env browser*/

import React, { ReactElement } from 'react'
import {
	Ref,
} from 'semantic-ui-react'
import { Controller, ControllerRenderProps, FieldError } from 'react-hook-form'

type Props = {
	name: string
	defaultValue?: string
	render(renderProps: Omit<ControllerRenderProps, 'ref'> & { error?: FieldError }): ReactElement
}

export const Question: React.FC<Props> = function (props) {
	const { render, name, defaultValue } = props

	return (
		<Controller
			name={name}
			defaultValue={defaultValue}
			render={({ field: { ref, ...rest }, fieldState: { error, } }) => (
				<Ref innerRef={ref}>{render({ error, ...rest })}</Ref>
			)}
		/>
	)
}
