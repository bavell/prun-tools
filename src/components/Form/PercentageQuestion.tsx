/*eslint-env browser*/

import React from 'react'
import { Form, Input, SemanticWIDTHS } from 'semantic-ui-react'
import { Question } from './Question'

type Props = {
	name: string
	placeholder?: string
	label?: string
	inputLabel?: string
	width?: SemanticWIDTHS
	step?: number
	inline?: boolean
	fluid?: boolean
	disabled?: boolean
}

export const PercentageQuestion: React.FC<Props> = function (props) {
	const { name, label, inputLabel, width, inline, ...rest } = props

	return (
		<Question
			name={name}
			render={({ error, ...fieldProps }) => (
				<Form.Field className='percentage-question' error={error?.message} width={width} inline={inline}>
					{!!label && <label>{label}</label>}
					{/* {console.log('raw + formatted input val', fieldProps.value, format(fieldProps.value))} */}
					<Input
						type={'number'}
						icon={'percent'}
						iconPosition={'left'}
						label={inputLabel}
						min={0}
						max={100}
						{...rest}
						{...fieldProps}
						value={format((fieldProps.value))}
						onChange={(e, field) => {
							let val = parse(field.value)
							// console.log('raw + parsed input val', val, normalize(val))
							fieldProps.onChange(normalize(val))
						}}
					/>
				</Form.Field>
			)}
		/>
	)
}


function format(val: string | number) {
	let rv = parse(val)
	return isNaN(rv) || rv === 0 ? '' : `${rv}`
}


function parse(val: string | number) {
	return parseInt(`${val}`.replace(/[^0-9]*/g, '').trim())
}


function normalize(num: number) {
	if (isNaN(num)) num = 0
	if (num > 100) num = 100
	if (num < 0) num = 0
	return num
}
