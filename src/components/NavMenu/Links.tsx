import React, { useState } from 'react'
import { Menu, Icon, MenuProps, SemanticICONS, IconProps, Image, Popup } from 'semantic-ui-react'
import { Link, NavLink } from 'react-router-dom'

import styles from './NavMenu.module.scss'

export const NavMenuLink: React.FC<{
	to: string
	text: string
	icon?: SemanticICONS
	id?: string
	className?: string
	imgURL?: string
	exact?: boolean
	raw?: boolean
}> = function (props) {
	const cls = ['item', props.className, styles.item].filter(x => !!x).join(' ')
	return (
		props.raw ? (
			<a id={props.id} className={cls} href={props.to}>
				<Icon name={props.icon} /> {props.text}
			</a>
		) : (
			<NavLink  id={props.id} className={cls} to={props.to}>
				<Icon name={props.icon} />
				{props.text}
			</NavLink>
		)
	)
}

export const NavMenuParentLink: React.FC<{
	text: string
	to?: string
	icon?: SemanticICONS
	children: React.ReactNode
	id?: string
}> = function (props) {
	const [open, setOpen] = useState(true)

	const tmp = props.to ? { to: props.to, as: NavLink } : {}
	return (
		<Menu.Item
			{...tmp}
			id={props.id}
			onClick={(e) => {
				e.preventDefault()
				e.stopPropagation()
				setOpen(!open)
				return false
			}}>
			{!open && <Icon name='chevron down' />}
			{props.text}
			{!!open && <Menu.Menu>{props.children}</Menu.Menu>}
		</Menu.Item>
	)
}

export const NavMenuChildLink: React.FC<{
	to: string
	text: string
	exact?: boolean
	icon?: SemanticICONS
	iconProps?: IconProps
	id?: string
}> = function (props) {
	return (
		<NavLink
			id={props.id}
			className='item'
			to={props.to}
			exact={props.exact}
			onClick={(e) => {
				e.stopPropagation()
			}}>
			{!!props.icon && <Icon name={props.icon} {...props.iconProps} />} {props.text}
		</NavLink>
	)
}
