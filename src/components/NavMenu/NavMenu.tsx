import React, { useState } from 'react'
import { Menu, Icon, MenuProps, SemanticICONS } from 'semantic-ui-react'

import { NavMenuChildLink, NavMenuLink, NavMenuParentLink } from './Links'
import { PageRouteNames, PageRoute, PageRouteIcons } from '@constants'

import styles from './NavMenu.module.scss'

type Props = {} & MenuProps
export const NavMenu: React.FC<Props> = function (props) {
	return (
		<Menu className={styles.navmenu} {...props}>
			<MenuLink route={PageRoute.HOME}/>
			<MenuLink route={PageRoute.MARKET}/>
			<MenuLink route={PageRoute.ORDERBOOK}/>
		</Menu>
	)
}

type MenuLinkProps = {
	route: PageRoute
}
const MenuLink: React.FC<MenuLinkProps> = function (props) {
	return (
		<NavMenuLink to={props.route} text={PageRouteNames[props.route]} icon={PageRouteIcons[props.route]} exact />
	)
}
